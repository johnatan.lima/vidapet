# VIDA PET

DESCRIÇÃO

## Pré-requisitos

Certifique-se de ter as seguintes ferramentas instaladas:

- Python (versão 3.8 ou mais recente)
- Flask (versão 2.3 ou mais recente)

## Instalação

1. Clone este repositório:

   ```shell
   $ git clone https://gitlab.com/SEUUSUARIO/vidapet.git
   ```

2. Acesse o diretório do projeto:

   ```shell
   $ cd vidapet
   ```

3. Crie e ative um ambiente virtual:

   ```shell
   $ python -m venv venv
   $ source venv/bin/activate
   ```

4. Instale as dependências:

   ```shell
   $ pip install -r requirements.txt
   ```

## Configuração

1. Renomeie o arquivo `.env.example` para `.env`.
2. Edite o arquivo `.env` e configure as variáveis de ambiente necessárias, como informações de banco de dados, chaves secretas, etc.

## Utilização

1. Inicie o servidor Flask:

   ```shell
   $ flask run
   ```

2. Acesse o aplicativo no seu navegador em `http://localhost:5000`.
